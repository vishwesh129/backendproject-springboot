package com.example.demo;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.micrometer.common.lang.NonNull;
import jakarta.persistence.*;
import lombok.Data;


@Entity
@Table(name = "Users")
@Data


public class User {
    @JsonProperty("id")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
 	private Long id;
    
    @JsonProperty("username")
	@NonNull
 	private String username;
	
    @JsonProperty("password")
	@NonNull
 	private String password;
 	
	// @Add generates getters, setters for your class automatically. You don't need to write them manually.
 	// @RequiredArgsConstructor generates constructors for your class automatically.
    // If we add @Data, then we don't need to add @Add and @RequiredArgsConstructor.
}